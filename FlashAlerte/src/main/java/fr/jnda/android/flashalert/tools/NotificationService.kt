package fr.jnda.android.flashalert.tools

import android.content.Context
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import fr.jnda.android.flashalert.FlashAlert
import fr.jnda.android.flashalert.db.AppDatabase
import fr.jnda.android.flashalert.db.AppEntryRepository
import fr.jnda.android.flashalert.impl.CameraImpl
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class NotificationService : NotificationListenerService() {

    private lateinit var mContext: Context
    lateinit var mCameraImpl: CameraImpl

    override fun onCreate() {
        super.onCreate()
        mContext = applicationContext
        mCameraImpl = CameraImpl.newInstance(mContext)
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        val pack = sbn.packageName



        GlobalScope.launch {
            if (DeviceController.continueEvent(mContext)) {

                val selectorDao = AppDatabase.INSTANCE?.appSelector()
                if (selectorDao != null) {
                    val appEntry = AppEntryRepository(selectorDao)
                    val item = appEntry.getItemByPackage(pack)
                    Log.d("FLASH", "=> $item")
                    if (item != null && item.selected) {
                        if (!FlashAlert.isRunning)
                            FlashAlert.isRunning = mCameraImpl.toggleStroboscope()

                        Timer().schedule(object : TimerTask() {
                            override fun run() {
                                if (FlashAlert.isRunning) {
                                    FlashAlert.isRunning = false
                                    mCameraImpl.stopStroboscope()
                                }
                            }
                        }, 1500)
                    }
                }
            }
        }
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {
        Log.i("Msg", "Notification Removed")

    }
}
