package fr.jnda.android.flashalert.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import fr.jnda.android.flashalert.MainActivity
import fr.jnda.android.flashalert.R
import fr.jnda.android.flashalert.impl.IPermissionCallBack
import fr.jnda.android.flashalert.tools.PermissionHelper


/**
 * @author x192697
 * @project FlashAlert
 *
 * Création 30/07/18
 *
 *
 */
class SettingsPreferenceFragment : androidx.preference.PreferenceFragmentCompat(),
        SharedPreferences.OnSharedPreferenceChangeListener,
        IPermissionCallBack {

    private val arrayKeys = arrayListOf("settings_all", "settings_contacts")
    private lateinit var permissionHelper: PermissionHelper
    private lateinit var mContext: Context
    private lateinit var mActivity: MainActivity


    override fun onResume() {
        super.onResume()
        val startSwitch = findPreference<SwitchPreference>("isActivate")
        startSwitch?.isChecked = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("isActivate", true)
    }
    override fun onPermissionEvent(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        for (i in permissions.indices) {
            Log.d("JNDA","Permissions " + permissions[i])
            when (permissions[i]) {
                "android.permission.RECEIVE_MMS",
                "android.permission.RECEIVE_SMS" -> {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        val switch = findPreference<SwitchPreference>("event_sms")
                        switch?.isChecked = false
                    }
                }
                "android.permission.READ_PHONE_STATE",
                "android.permission.READ_CALL_LOG" -> {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        val switch = findPreference<SwitchPreference>("event_call")
                        switch?.isChecked = false
                    }
                }
                "android.permission.READ_CONTACTS" -> {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        val switch = findPreference<SwitchPreference>("settings_contacts")
                        switch?.isChecked = false
                    } else {
                        disableOther("settings_contacts")
                    }
                }
                "android.permission.BIND_NOTIFICATION_LISTENER_SERVICE" -> {
                    val notification = findPreference<Preference>("event_apps")
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        notification?.summary = getString(R.string.notify_list_apps)
                    } else {
                        notification?.summary = getString(R.string.notify_grant_permission)
                    }
                }
            }
        }
    }

    @SuppressLint("InflateParams")
    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if (p0 != null && p1 != null) {
            val checked = p0.getBoolean(p1, false)

            when {
                p1 == "settings_dayAndNight" -> {
                    mActivity.changeNightStyle(checked)
                }
                p1 == "event_call" && checked -> {
                    if (!permissionHelper.callHasPermission())
                        permissionHelper.askPermission(Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALL_LOG)
                }
                p1 == "event_sms" && checked -> {
                    if (!permissionHelper.smsHasPermission())
                        permissionHelper.askPermission(Manifest.permission.RECEIVE_SMS, Manifest.permission.RECEIVE_MMS)
                }
                p1 == "settings_contacts" && checked -> {
                    if (!permissionHelper.contactsHasPermission())
                        permissionHelper.askPermission(Manifest.permission.READ_CONTACTS)
                    else
                        disableOther(p1)
                }
                p1 == "isActivate" -> {
                    val startSwitch = findPreference<SwitchPreference>("isActivate")
                    startSwitch?.isChecked = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("isActivate", true)
                }
                checked -> disableOther(p1)
            }

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        mActivity = context as MainActivity

        PreferenceManager.getDefaultSharedPreferences(mContext).registerOnSharedPreferenceChangeListener(this)

    }

    override fun onDetach() {
        super.onDetach()
        PreferenceManager.getDefaultSharedPreferences(mContext).unregisterOnSharedPreferenceChangeListener(this)

    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        permissionHelper = PermissionHelper(activity!!)
        if (!permissionHelper.cameraHasPermission()) {
            permissionHelper.displayInfo()
        }

        addPreferencesFromResource(R.xml.pref_event)
        addPreferencesFromResource(R.xml.pref_settings)
        addPreferencesFromResource(R.xml.pref_other)
        val switchSMS = findPreference<SwitchPreference>("event_sms")
        val switchCall = findPreference<SwitchPreference>("event_call")
        val switchContact = findPreference<SwitchPreference>("settings_contacts")
        switchSMS?.isChecked = (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("event_sms", false) && permissionHelper.smsHasPermission())
        switchCall?.isChecked = (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("event_call", false) && permissionHelper.callHasPermission())
        switchContact?.isChecked = (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("settings_contacts", false) && permissionHelper.contactsHasPermission())

       findPreference<Preference>("event_apps")?.let {
            val granted = permissionHelper.canReadNotifications()

            if(granted)
                it.summary = getString(R.string.notify_list_apps)
            else
                it.summary = getString(R.string.notify_grant_permission)

            it.setOnPreferenceClickListener{
                if (permissionHelper.canReadNotifications()) {
                    mActivity.supportFragmentManager.beginTransaction().replace(android.R.id.content, AppListPreferenceFragment(), "NOTIFYAPP").addToBackStack("NOTIFYAPP").commit()
                    mActivity.invalidateOptionsMenu()
                } else {
                    permissionHelper.buildNotificationServiceAlertDialog().show()
                }
                true
            }
        }


    }

    private fun disableOther(str: String) {
        if (arrayKeys.contains(str)) {

            arrayKeys.filter { it != str }.forEach {
                val switch = findPreference<SwitchPreference>(it)
                switch?.isChecked = false
            }
        }
    }
}